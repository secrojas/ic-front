import { useState } from "react";
import TaskService from "../services/TaskService";


const AddTask = () => {
    const initialTaskState = {
        id: null,
        name: ''
    };

    const [task, setTask] = useState(initialTaskState);
    const [submitted, setSubmitted] = useState(false);

    const handleInputChange = event => {
        const {name, value} = event.target;
        setTask({ ...task, [name]: value });
    }

    const saveTask = () => {
        let data = {
            name: task.name
        }

        TaskService.create(data)
        .then(response => {
            setTask({
                id: response.data.id,
                name:response.data.name
            });
            setSubmitted(true);
            console.log(task);   
        })
        .catch(err => {
            alert('Error!');
            console.log(err);
        });
    }    

    const newTask = () => {
        setTask(initialTaskState);
        setSubmitted(false);
    }

    return (
        <div className="submit-form">
            {submitted ? (
                <div>
                    <h4>Task created success</h4>
                    <button className="btn btn-primary" onClick={newTask}>Create new one</button>
                </div>
            ) : (
                <div>
                    <div className="form-group">
                        <label>Name of the task</label>
                        <input 
                            type="text"
                            className="form-control"
                            id="name"
                            required
                            value={task.name}
                            onChange={handleInputChange}
                            name="name"
                        />
                    </div>
                    <br />
                    <button onClick={saveTask} className="btn btn-success">
                        Save Task
                    </button>
                </div>
            )}            
        </div>
    )
};

export default AddTask;