import { useEffect, useState } from "react";
import TaskService from "../services/TaskService";

const Task = (props) => {

    const initialTaskState = {
        id: null,
        name: ''
    };

    const [currentTask, setCurrentTask] = useState(initialTaskState);
    const [message, setMessage] = useState('');

    const getTask = (id) => {
        TaskService.getById(id)
        .then(response => {
            setCurrentTask(response.data.data);
        })
        .catch(err => {
            alert('Error!');
            console.log(err);
        });
    }

    useEffect(() => {
        getTask(props.match.params.id)
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const {name, value} = event.target;
        setCurrentTask({ ...currentTask, [name]: value });
    }

    const updateTask = () => {
        TaskService.update(currentTask.id, currentTask)
        .then(response => {
            setMessage('Task updated');  
        })
        .catch(err => {
            setMessage('Error trying to update task'); 
            console.log(err);
        });
    } 
    
    const redirectHome = () => {
       props.history.push('/');
    }

    const deleteTask = () => {
        if( !window.confirm('Are yo sure to delete the task?')) {
            return;
        }

        TaskService.remove(currentTask.id)
        .then(response => {
            props.history.push('/');
        })
        .catch(err => {
            setMessage('Error trying to delete task'); 
            console.log(err);
        });
    }

    return (
        <div className="submit-form">
            { !currentTask ? (
                <div>
                    <h4>You need to select a task</h4>
                </div>
            ) : (
                <div>
                    <div className="form-group">
                        <label>Name of the task</label>
                        <input 
                            type="text"
                            className="form-control"
                            id="name"
                            required
                            defaultValue={currentTask.name}
                            onChange={handleInputChange}
                            name="name"
                        />
                    </div>
                    <br />
                    <button onClick={updateTask} className="btn btn-success">
                        Save Task
                    </button>
                    <button onClick={deleteTask} className="btn btn-danger">
                        Delete Task
                    </button>
                    <div className="mt-5">
                        <span className="badge text-bg-primary">{message}</span>
                    </div>
                    <button onClick={redirectHome} className="mt-3 btn btn-dark">
                        Go Back
                    </button>
                </div>
            )}            
        </div>
    )
};

export default Task;