import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import TaskService from "../services/TaskService";

const TaskList = () => {
    const [tasks, setTasks] = useState([]);
    const [currentTask, setCurrentTask] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);

    useEffect(() => {
        retrieveTasks();
    }, []);

    const retrieveTasks = () => {
        TaskService.getAll()
        .then(response => {
            setTasks(response.data.data);
            console.log(response.data.data);
        })
        .catch(err => {
            alert('Error!');
            console.log(err);
        });
    }

    const refreshList = () => {
        retrieveTasks();
    }

    const setActiveTask = (task, index) => {
        setCurrentTask(task);
        setCurrentIndex(index);
    }

    return (
        <div className="row">
           <div className="col-6">
                <h4>Tasks</h4>
                <table className="table">
                    <thead>
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Completed</th>
                        <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tasks &&
                            tasks.map((task, index) => (
                                <tr key={index}>
                                    <th>{task.id}</th>
                                    <td>{task.name}</td>
                                    <td style={{textAlign:'center'}}>
                                        {task.completed ? (
                                            <span className="badge text-bg-success">yes</span>
                                            ) : (
                                                <span className="badge text-bg-warning">no</span> 
                                        )}
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <i className="bi bi-info-square-fill" onClick={() => setActiveTask(task, index)}> </i>
                                        <Link className="bi bi-pencil" to={'/tasks/' + task.id}> </Link>
                                    </td>
                                </tr>
                            ))
                        }                                               
                    </tbody>
                </table>
           </div>
           <div className="col-6">
            {(currentTask) ? (                
                <div className="mt-5" style={{textAlign:'center'}}>
                    <h4>{currentTask.name}</h4>
                    <div>                        
                        {currentTask.completed ? (
                           <span className="badge text-bg-success">Completed</span>
                        ) : (
                            <span className="badge text-bg-warning">Not Completed</span> 
                        )} 
                    </div>
                    <Link to={'/tasks/' + currentTask.id} className="badge badge-warning">Edit</Link>
                </div>               
            ) : (
                <div className="mt-5" style={{textAlign:'center'}}>
                    <br />
                    <span className="badge text-bg-dark">You need to select a task...</span>
                </div>
            )}
           </div>
        </div>
    )
};

export default TaskList;