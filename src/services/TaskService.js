import http from '../http-common';

const getAll = () => {
    return http.get('tasks');
}

const getById = (id) => {
    return http.get(`tasks/${id}`)
}

const create = (data) => {
    return http.post('tasks', data);
}

const update = (id, data) => {
    return http.put(`tasks/${id}`, data);
}

const remove = (id) => {
    return http.delete(`tasks/${id}`);
}

export default {
    getAll,
    getById,
    create,
    update,
    remove
}
