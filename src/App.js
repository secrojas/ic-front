import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import { Link, Route, Switch } from 'react-router-dom';
import TaskList from './components/TaskList';
import AddTask from './components/AddTask';
import Task from './components/Task';

function App() {
  return (
    <div>
        <nav className="navbar navbar-expand-lg bg-light">
          <div className="container-fluid">
            <Link className="navbar-brand" to={"/"}>Info Casas</Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link className="nav-link" to={"/tasks"}>Tasks</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={"/add"}>Add</Link>
                </li>
               
                
              </ul>
              {/* <form className="d-flex" role="search">
                <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                <button className="btn btn-outline-success" type="submit">Search</button>
              </form> */}
            </div>
          </div>
        </nav>
        <div className='container mt-3'>
          <Switch>
            <Route exact path={["/", "/tasks"]} component={TaskList}></Route>
            <Route exact path={"/add"} component={AddTask}></Route>
            <Route exact path={"/tasks/:id"} component={Task}></Route>
          </Switch>
        </div>
    </div>
  );
}

export default App;
